#define ANNDLL_EXPORTS
#include <iostream>
#include <ANN.h>

namespace ANN
{
	class Network : public ANeuralNetwork // ����� ����� �������
	
	{

	protected: // � ������� �����������

		float moment; // ������

	public: // �����

		Network(
			std::vector<int> & configuration, // ����� ����� ���� ������� ��������� � ������������
			ANeuralNetwork::ActivationType activation_type) // ������� ���������
		{
			this->configuration = configuration; // �������� ������������ � ��� ����� �� ������ ANN
			
			for (int i = 0; i < this->configuration.size(); i++)
			{
				++this->configuration[i];
			}

						
			this->activation_type = activation_type; // �������� ������� ��������� � ��� ����� �� ������ ANN
			this->scale = 1;
			this->moment = 0.1;
		}

		ANNDLL_API std::string GetType(); // ��������� ������ �������� ����

		ANNDLL_API std::vector<float> Predict(std::vector<float> & input);//��������������� ������ �� ���������� �����

		ANNDLL_API float BackPropTrain( //�������� ���� ������� ��������� ��������������� ������
			std::shared_ptr<ANN::ANeuralNetwork>ann,
			std::vector<std::vector<float>> & inputs, // ����� ��� ��������
			std::vector<std::vector<float>> & outputs,// ������ ��� ��������

			int max_iters = 10000, // ������������ ���������� �������� ��� ��������
			float eps = 0.1, // ������� ������ �� ���� �������� ��� ������� ���������� ��������� ��������
			float speed = 0.1, // �������� ��������
			bool std_dump = false // �������� �� ���� �� ����� 
			);

	private: // ������ ������ ������

		void ForwardPropagate(std::vector < float > & input,// ������ ���� ������ 
			std::vector < std::vector < float > > & output_all); //����� ������� �������

		void BackwardPropagate(std::vector<float> & input, // ������ ���� �����
			std::vector<float> & delta_0,
			std::vector < std::vector < float > > & output_all, // ��� ��������� �������
			std::vector < std::vector < std::vector < float > > > & delta_i, // ��� ���� ��������� ��������
			float speed);
	};
}

std::shared_ptr<ANN::ANeuralNetwork> ANN::CreateNeuralNetwork( // ������� ��������� ����
	std::vector<int> & configuration,
	ANeuralNetwork::ActivationType activation_type)
{
	return std::make_shared<ANN::Network>(configuration, activation_type); // ����������� ������
}

std::string ANN::Network::GetType() // ������� ���������� � ���� ��������� ����
{
	return "Network by Komarova Evseniia";
}

std::vector<float> ANN::Network::Predict(std::vector<float> & input) //��������������� ������ �� ���������� ����� (���������� ����� ���������� �������)
{
	std::vector < float > in = input;
	std::vector < float > out;
	for (int i = 0; i < weights.size(); i++) // ���� �� �����
	{
		out.resize(weights[i].size() - 1); // -1 ��-�� ������� ��������, � ���� ��� ������ (11-1) �����
		for (int j = 0; j < weights[i].size() - 1; j++) // ���� �� �������� (11-1)
		{
			float neuron = weights[i][j][in.size()];// in - ���������� �������� �� ������ ���� (������ ��������)
			for (int k = 0; k < in.size(); k++)
			{
				neuron += in[k] * weights[i][j][k];
			}
			out[j] = Activation(neuron);
		}
		in.swap(out); // ����� ������ ������
	}
	return in;
}

float ANN::Network::BackPropTrain //�������� ���� ������� ��������� ��������������� ������
(
std::shared_ptr<ANN::ANeuralNetwork>ann,//����
std::vector < std::vector < float > > & inputs, // ����� ������
std::vector < std::vector < float > > & outputs,
int max_iters,
float eps,
float speed,
bool std_dump
)
{
	std::vector < std::vector < std::vector < float > > > weight_deltas; // ������ �����
	weight_deltas.resize(configuration.size() - 1); // �� ��������� ������ ������� ������ ��� � ��� ��� ���� (��������� ���-�� �����) (������ 3)
	weights.resize(configuration.size() - 1); // ������ ������ ������� ����� (������ ������� 3)
	for (int i = 0; i < weights.size(); i++) // ���� �� ����� 
	{
		weights[i].resize(configuration[i + 1]); // �������� ������ ���������� ���� (11)(11)(2) (����������� ������ ��������) ���������� �����
		weight_deltas[i].resize(configuration[i + 1]); // ���� �����
		for (int j = 0; j < weights[i].size(); j++) // ���� �� ��������
		{
			weights[i][j].resize(configuration[i]); // ������ ���-�� ������ 
			weight_deltas[i][j].resize(configuration[i]); // ���� �����
			if ((j + 1) != weights[i].size()) //  ���� �� ������ ��������, �� (��������� ��� �������, ����� ������� ��������)
			{
				for (int k = 0; k < weights[i][j].size(); k++) // ���� �� �����
				{
					weights[i][j][k] = (float)rand() / RAND_MAX; // ������ �������� ���� 
				}
			}
		}
	}

	float err; // ������
	int iterations = 0; // ���-�� ��������

	do // �������� ������� ����
	{
		err = 0;
		for (int i = 0; i < inputs.size(); i++) // 4 �������� (�� ���-�� �����)
		{
			std::vector < std::vector < float > > layer_outputs(weights.size()); // ����� ������� ������� (���-�� �����)

			ForwardPropagate(inputs[i], layer_outputs); // �������� ������ 

			std::vector < float > & output = layer_outputs.back(); // �������� ����� ���������� �������

			std::vector < float > delta_0(output.size()); //  ������ ������ ��� ��������� �������

			for (int j = 0; j < output.size(); j++)
			{
				float diff = outputs[i][j] - output[j]; // ����� ��� - ���� ������
				err += diff * diff; // ������������� ������
				delta_0[j] = diff * ActivationDerivative(output[j]); // �������� ������ ������
			}

			BackwardPropagate(inputs[i], delta_0, layer_outputs, weight_deltas, speed); // �������� ����� (������ ������)
		}
		if (std_dump && ((iterations % (max_iters / 100)) == 0)) // ����� �������� 
		{
			std::cout << iterations << ": " << err << std::endl;
		}
		iterations++;
	} 
	while ((err > eps) && (iterations < max_iters));

	is_trained = true; // ���� ������� (���� �� ������� ����, �� ���� �� ������������)

	if (std_dump)
	{
		std::cout << iterations << ": " << err << std::endl; // �������� ��������� (���� ���-����������) � �������� ������ �� ���� ��������
	}

	return err;
}
void ANN::Network::ForwardPropagate(std::vector<float> & input, // ������� ���� ������ (���������� ������ ������� �������)
	std::vector < std::vector < float > > & output_all)
{
	std::vector < float > in = input;
	std::vector < float > out;

	for (int i = 0; i < weights.size(); i++)
	{
		out.resize(weights[i].size() - 1);
		output_all[i].resize(weights[i].size() - 1);
		for (int j = 0; j < weights[i].size() - 1; j++)
		{
			float neuron_input = weights[i][j][in.size()];
			for (int k = 0; k < in.size(); k++)
			{
				neuron_input += in[k] * weights[i][j][k];
			}
			output_all[i][j] = Activation(neuron_input);
			out[j] = output_all[i][j];
		}
		in.swap(out);
	}
}

void ANN::Network::BackwardPropagate(std::vector<float> & input, // �������� �����
	std::vector<float> & delta_0,
	std::vector < std::vector < float > > & output_all,
	std::vector < std::vector < std::vector < float > > > & delta_i,
	float speed)
{
	std::vector < float > in = delta_0; // ������ - �����
	std::vector < float > out;

	for (int i = weights.size() - 1; i-- > 0;) // ���� �� ����� (������ ��� �������������� ����) 
	{
		out.resize(weights[i].size() - 1); // 10-1 � ������� ������� ��� ������
		for (int j = 0; j < weights[i].size() - 1; j++) //���� �� ��������
		{
			float delta_neuron = 0;
			for (int k = 0; k < in.size(); k++) // ���� �� ������
			{
				delta_neuron += in[k] * weights[i + 1][k][j]; // i+1 ����� ���� � ���������� ���� ( ����� ������������ ����� �� ������)
			}
			delta_neuron *= ActivationDerivative(output_all[i][j]); // �������� �� ����������� �� ����� ������
			out[j] = delta_neuron;
		}
		for (int k = 0; k < in.size(); k++) // �� ������
		{
			for (int j = 0; j < weights[i].size() - 1; j++) //10-1 �������
			{
				delta_i[i + 1][k][j] = speed * output_all[i][j] * in[k] + moment * delta_i[i + 1][k][j];
				weights[i + 1][k][j] += delta_i[i + 1][k][j]; // ���������� ������ � ������� ���� � �������� �����
			}
			delta_i[i + 1][k][weights[i + 1][k].size() - 1] = speed * in[k] + moment * delta_i[i + 1][k][weights[i + 1][k].size() - 1]; // ��� ������� ��������
			weights[i + 1][k][weights[i + 1][k].size() - 1] += delta_i[i + 1][k][weights[i + 1][k].size() - 1];
		}
		in.swap(out);
	}
	for (int k = 0; k < in.size(); k++) // ��� ������� �������� (�� ��������)
	{
		for (int j = 0; j < input.size(); ++j) // �� ������
		{
			delta_i[0][k][j] = speed * input[j] * in[k] + moment * delta_i[0][k][j];
			weights[0][k][j] += delta_i[0][k][j];
		}
		delta_i[0][k][weights[0][k].size() - 1] = speed * in[k] + moment * delta_i[0][k][weights[0][k].size() - 1]; //������ ��������
		weights[0][k][weights[0][k].size() - 1] += delta_i[0][k][weights[0][k].size() - 1];
	}
}